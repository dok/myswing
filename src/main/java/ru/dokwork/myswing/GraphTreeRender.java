package ru.dokwork.myswing;

import java.awt.Color;
import java.awt.Component;
import java.awt.Graphics;
import java.awt.Point;
import java.awt.Rectangle;

import javax.swing.JComponent;
import javax.swing.plaf.ComponentUI;

/**
 * Класс для отрисовки дерева.
 * 
 * @author dok
 * 
 */
class GraphTreeRender extends ComponentUI {

    @Override
    public void paint(Graphics g, JComponent c) {
        if (c instanceof JGraphTree) {
            JGraphTree tree = (JGraphTree) c;
            Object rootNode = tree.getModel().getRoot();
            paintEdge(null, rootNode, tree, g);
            paintNode(rootNode, tree, g);
        }
    }

    public void paintNode(Object node, Graphics g, Rectangle boundary,
            boolean pressed, boolean highlighted) {
        Component c = nodeRender.getNodeRendererComponent(node, pressed, highlighted);
        g = g.create(boundary.x, boundary.y, boundary.width, boundary.height);
        c.setBounds(boundary);
        c.paint(g);
    }

    /**
     * Метод рекуретной отрисовки узлов.
     * 
     * @param node
     *            корневой узел в дереве.
     * @param tree
     *            рисуемое дерево.
     * @param g
     */
    private void paintNode(Object node, JGraphTree tree, Graphics g) {
        Rectangle nodeBoundary = tree.treeMap.getNodeBoundary(node);
        paintNode(node, g, nodeBoundary, node == tree.pressedNode,
                node == tree.highlightedNode);
        for (int i = 0; i < tree.getModel().getChildCount(node); i++) {
            Object child = tree.getModel().getChild(node, i);
            paintNode(child, tree, g);
        }
    }

    /**
     * Метод рекуретного отображения ребер дерева.
     * 
     * @param node
     *            корневой узел в дереве.
     * @param tree
     *            рисуемое дерево.
     * @param g
     */
    private void paintEdge(Object parent, Object node, JGraphTree tree, Graphics g) {
        if (parent != null) {
            Point rootCenter = getCenter(tree.treeMap.getNodeBoundary(parent));
            Point nodeCenter = getCenter(tree.treeMap.getNodeBoundary(node));
            g.setColor(color);
            g.drawLine(rootCenter.x, rootCenter.y, nodeCenter.x, nodeCenter.y);
        }
        for (int i = 0; i < tree.getModel().getChildCount(node); i++) {
            Object child = tree.getModel().getChild(node, i);
            paintEdge(node, child, tree, g);
        }

    }

    GraphTreeRender(NodeRender nodeRender) {
        this.nodeRender = nodeRender;
    }

    /** Возвращает центр прямоугольника */
    private Point getCenter(Rectangle rect) {
        Point p = new Point();
        p.x = rect.x + rect.width / 2;
        p.y = rect.y + rect.height / 2;
        return p;
    }

    protected NodeRender nodeRender;

    private Color color = Color.BLACK;
}
