package ru.dokwork.myswing;

/**
 * Абстрактный класс для обработки событий мыши. Методы этого класса пусты. Класс
 * создан для упрощения создания слушателей событий взаимодействия с узлами дерева.
 * 
 * @author dok
 * 
 */
public abstract class MouseNodeAdapter implements MouseNodeEventListener {

    /**
     * {@inheritDoc}
     */
    @Override
    public void mousePressed(MouseNodeEvent event) {
    }

    /**
     * {@inheritDoc}
     */
    @Override
    public void mouseReleased(MouseNodeEvent event) {
    }

    /**
     * {@inheritDoc}
     */
    @Override
    public void mouseEntered(MouseNodeEvent event) {
    }

    /**
     * {@inheritDoc}
     */
    @Override
    public void mouseExited(MouseNodeEvent event) {
    }

    /**
     * {@inheritDoc}
     */
    @Override
    public void mouseClicked(MouseNodeEvent event) {
    }

}
