package ru.dokwork.myswing;

import java.awt.EventQueue;

import javax.swing.UIManager;
import javax.swing.UIManager.LookAndFeelInfo;
import javax.swing.tree.DefaultMutableTreeNode;
import javax.swing.tree.DefaultTreeModel;
import javax.swing.tree.TreeModel;

/**
 * Запускает демонстрационное приложение.
 * 
 * @author dok
 * 
 */
public class App implements Runnable {
    /**
     * Launch the application.
     */
    public static void main(String[] args) {
        try {
            UIManager.setLookAndFeel(getLookAndFeel());
        } catch (Exception e) {
            e.printStackTrace();
        }
        EventQueue.invokeLater(new App());
    }

    private static String getLookAndFeel() {
        for (LookAndFeelInfo info : UIManager.getInstalledLookAndFeels()) {
            if ("Nimbus".equals(info.getName())) {
                return info.getClassName();
            }
        }
        return UIManager.getSystemLookAndFeelClassName();
    }

    public void run() {
        final DemoWindow window = new DemoWindow();
        TreeModel model = createTreeModel();
        window.graphTree.setModel(model);
        window.graphTree.addMouseNodeEventListener(new MouseNodeAdapter() {
            @Override
            public void mouseClicked(MouseNodeEvent event) {
                DefaultMutableTreeNode node = (DefaultMutableTreeNode) event.getNode();
                String text = null;
                if (node.getUserObject() instanceof BookInfo) {
                    BookInfo bi = (BookInfo) node.getUserObject();
                    text = bi.bookName + ": " + bi.bookURL;
                } else {
                    text = node.getUserObject().toString();
                }
                window.labelComment.setText(text);
            }
        });
        window.frmJgraphtreeDemo.setVisible(true);
    }

    private TreeModel createTreeModel() {
        DefaultMutableTreeNode top = new DefaultMutableTreeNode("The Java Series");
        createNodes(top);
        return new DefaultTreeModel(top);
    }

    /**
     * @see http://docs.oracle.com/javase/tutorial/uiswing/components/tree.html
     */
    private void createNodes(DefaultMutableTreeNode top) {
        DefaultMutableTreeNode category = null;
        DefaultMutableTreeNode book = null;

        category = new DefaultMutableTreeNode("Books for Java Programmers");
        top.add(category);

        // original Tutorial
        book = new DefaultMutableTreeNode(new BookInfo(
                "The Java Tutorial: A Short Course on the Basics", "tutorial.html"));
        category.add(book);

        // Tutorial Continued
        book = new DefaultMutableTreeNode(new BookInfo(
                "The Java Tutorial Continued: The Rest of the JDK", "tutorialcont.html"));
        category.add(book);

        // Swing Tutorial
        book = new DefaultMutableTreeNode(new BookInfo(
                "The Swing Tutorial: A Guide to Constructing GUIs", "swingtutorial.html"));
        category.add(book);

        // ...add more books for programmers...

        category = new DefaultMutableTreeNode("Books for Java Implementers");
        top.add(category);

        // VM
        book = new DefaultMutableTreeNode(new BookInfo(
                "The Java Virtual Machine Specification", "vm.html"));
        category.add(book);

        // Language Spec
        book = new DefaultMutableTreeNode(new BookInfo("The Java Language Specification",
                "jls.html"));
        category.add(book);
    }

    private class BookInfo {
        public String bookName;
        public String bookURL;

        public BookInfo(String book, String url) {
            bookName = book;
            bookURL = url;
        }

        public String toString() {
            return bookName;
        }
    }
}
