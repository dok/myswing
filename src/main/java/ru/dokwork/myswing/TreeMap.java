package ru.dokwork.myswing;

import java.awt.Dimension;
import java.awt.Point;
import java.awt.Rectangle;
import java.beans.PropertyChangeListener;
import java.beans.PropertyChangeSupport;
import java.util.HashMap;
import java.util.Iterator;
import java.util.Map;

import javax.swing.tree.TreeModel;

/**
 * Описывает расположение узлов дерева.
 * 
 * @author dok
 * 
 */
public class TreeMap {

    /** Событие, которое генерируется каждый раз при перерасчете карты дерева. */
    public static final String TREE_CHANGED_EVENT = "TreeMap_changed";

    /**
     * Возвращает область отображения узла.
     * 
     * @param userNode
     *            узел дерева.
     * @return область отображения узла.
     */
    public Rectangle getNodeBoundary(Object userNode) {
        if (treeModel == null) {
            return null;
        }
        return map.get(userNode).getNodeBoundary();
    }

    /**
     * Возвращает размер области для отображения дерева.
     * 
     * @return размер области для отображения дерева.
     */
    public Dimension getSize() {
        return size;
    }

    /**
     * Возвращает узел дерева в который попадает указанная точка или null, если точка не
     * попадает ни в один из узлов.
     * 
     * @param p
     *            точка.
     * @return узел дерева или null.
     */
    public Object getNode(Point p) {
        if (treeModel == null) {
            return null;
        }
        Object rootNode = treeModel.getRoot();
        Bunch m = map.get(rootNode);
        return getTreeNode(p, m);
    }

    /**
     * Возвращает описание дерева.
     * 
     * @return модель дерева.
     */
    public TreeModel getTreeModel() {
        return treeModel;
    }

    /**
     * Устанавливает модель дерева.
     * 
     * @param treeModel
     *            модель дерева.
     */
    public void setTreeModel(TreeModel treeModel) {
        this.treeModel = treeModel;
        recalculate();
    }

    /**
     * Возвращает размер ячеек.
     */
    public Dimension getNodeSize() {
        return nodeSize;
    }

    /**
     * Устанавливает размер ячеек. Приводит к перерасчету всей карты.
     * 
     * @param nodeSize
     *            размер ячейки.
     */
    public void setNodeSize(Dimension nodeSize) {
        this.nodeSize = nodeSize;
        recalculate();
    }

    /**
     * Возвращает вертикальное расстояние между узлами дерева.
     */
    public int getVerticalMargin() {
        return verticalMargin;
    }

    /**
     * Устанавливает вертикальное расстояние между узлами дерева. Приводит к перерасчету
     * всей карты.
     * 
     * @param verticalMargin
     *            вертикальное расстояние между узлами дерева.
     */
    public void setVerticalMargin(int verticalMargin) {
        this.verticalMargin = verticalMargin;
        recalculate();
    }

    public int getHorizontalMargin() {
        return horizontalMargin;
    }

    /**
     * Устанавливает горизонтальное расстояние между узлами дерева. Приводит к перерасчету
     * всей карты.
     * 
     * @param horizontalMargin
     *            горизонтальное расстояние между узлами дерева.
     */
    public void setHorizontalMargin(int horizontalMargin) {
        this.horizontalMargin = horizontalMargin;
        recalculate();
    }

    public void addPropertyChangeListener(PropertyChangeListener listener) {
        event.addPropertyChangeListener(listener);
    }

    public void removePropertyChangeListener(PropertyChangeListener listener) {
        event.removePropertyChangeListener(listener);
    }

    /**
     * Создает описание расположения узлов дерева.
     */
    public TreeMap() {
        initialize();
    }

    /**
     * Создает описание расположения узлов дерева.
     * 
     * @param model
     *            описываемая модель дерева.
     */
    public TreeMap(TreeModel model) {
        initialize();
        setTreeModel(model);
    }

    /**
     * Устанавливаются значения поумолчанию
     */
    private void initialize() {
        this.event = new PropertyChangeSupport(this);
        this.nodeSize = new Dimension(30, 30);
        this.horizontalMargin = 5;
        this.verticalMargin = 20;
    }
    
    /**
     * Выполняет перерасчет карты дерева.
     */
    private void recalculate() {
        if (treeModel == null) {
            size = new Dimension(0, 0);
            event.firePropertyChange(TREE_CHANGED_EVENT, null, this);
            return;
        }
        map = new HashMap<Object, Bunch>();
        Object rootNode = treeModel.getRoot();
        size = addNodeToMap(rootNode, new Point(0, 0)).getExternalBoundary().getSize();
        event.firePropertyChange(TREE_CHANGED_EVENT, null, this);
    }

    /**
     * Рекурентно создает грозди для узла и его потомков, добавляя грозди в карту.
     * 
     * @param userNode
     *            узел дерева.
     * @param p
     *            координата верхнего левого узла грозди.
     * @return гроздь созданную для переданного узла.
     */
    private Bunch addNodeToMap(Object userNode, Point p) {
        Bunch bunch = new Bunch(p, userNode, nodeSize, new Dimension(horizontalMargin,
                verticalMargin));
        map.put(userNode, bunch);
        if (!treeModel.isLeaf(userNode)) {
            p.y += nodeSize.height + verticalMargin;
            for (int i = 0; i < treeModel.getChildCount(userNode); i++) {
                Object child = treeModel.getChild(userNode, i);
                Bunch m = addNodeToMap(child, new Point(p));
                bunch.addChild(m);
                p.x += m.getExternalBoundary().width + horizontalMargin;
            }
        }
        return bunch;
    }

    /**
     * Выполняет рекурсивный поиск узла, содержащего указанную точку.
     * 
     * @param p
     *            точка на дереве.
     * @param b
     *            гроздь, в которой выполняется поиск.
     * @return узел, в который попадает точка или null, если точка не попадает ни в какой
     *         из узлов.
     */
    private Object getTreeNode(Point p, Bunch b) {
        if (!b.getExternalBoundary().contains(p)) {
            return null;
        }
        if (b.getNodeBoundary().contains(p)) {
            return b.getUserNode();
        }
        Iterator<Bunch> itr = b.children();
        while (itr.hasNext()) {
            Bunch child = itr.next();
            Object node = getTreeNode(p, child);
            if (node != null) {
                return node;
            }
        }
        return null;
    }

    private TreeModel treeModel;

    private Map<Object, Bunch> map;

    private Dimension nodeSize;

    private Dimension size;

    private int verticalMargin;

    private int horizontalMargin;

    private PropertyChangeSupport event;
}
