package ru.dokwork.myswing;

import java.awt.Dimension;
import java.awt.Point;
import java.awt.Rectangle;
import java.util.ArrayList;
import java.util.Iterator;
import java.util.List;

/**
 * Гроздь (карта узла). Содержит область для отображения самого узла, а также список карт
 * его потомков. Выполняет важную функцию вычисления размеров грозди (узел и его потомки).
 * 
 * @author dok
 * 
 */
class Bunch {

    /**
     * Возвращает ссылку на узел дерева.
     */
    public Object getUserNode() {
        return userNode;
    }

    /**
     * Возвращает границу узла дерева. Узел должен отрисовываться в рамках этой границы.
     */
    public Rectangle getNodeBoundary() {
        return nodeBoundary;
    }

    /**
     * Возвращает границу грозди.
     */
    public Rectangle getExternalBoundary() {
        return externalBoundary;
    }

    /**
     * Возвращает итератор по списку потомков узла, представленных гроздями.
     */
    public Iterator<Bunch> children() {
        return children.iterator();
    }

    /**
     * Добавляет гроздь в список потомков.
     * 
     * @param child
     */
    public void addChild(Bunch child) {
        /* Recalculate width */
        if (children.size() == 0) {
            externalBoundary.width = child.getExternalBoundary().width;
        } else {
            externalBoundary.width += child.getExternalBoundary().width + margin.width;
        }
        nodeBoundary.x = externalBoundary.x + externalBoundary.width / 2
                - nodeBoundary.width / 2;

        /* Recalculate height */
        if (child.getExternalBoundary().height > maxChildHeight) {
            maxChildHeight = child.getExternalBoundary().height;
        }
        externalBoundary.height = nodeBoundary.height + margin.height + maxChildHeight;

        children.add(child);
    }

    /**
     * Создает гроздь.
     * 
     * @param p
     *            верхний левый угол грозди.
     * @param dim
     *            размер области для изображения узла.
     * @param margin
     *            отступы между узлами.
     */
    public Bunch(Point p, Object userNode, Dimension dim, Dimension margin) {
        this.userNode = userNode;
        this.margin = margin;
        externalBoundary = new Rectangle(p, dim);
        nodeBoundary = new Rectangle(p, dim);
        children = new ArrayList<Bunch>();
    }

    private Object userNode;

    private Rectangle nodeBoundary;

    private Rectangle externalBoundary;

    private Dimension margin;

    private List<Bunch> children;

    private int maxChildHeight = 0;
}
