package ru.dokwork.myswing;

/**
 * Реагирует на события взаимодествия мыши с узлом дерева.
 * 
 * @author dok
 * 
 */
public interface MouseNodeEventListener {

    void mousePressed(MouseNodeEvent event);

    void mouseReleased(MouseNodeEvent event);
    
    void mouseEntered(MouseNodeEvent event);
    
    void mouseExited(MouseNodeEvent event);

    void mouseClicked(MouseNodeEvent event);
}
