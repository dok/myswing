package ru.dokwork.myswing;

import java.awt.Color;
import java.awt.Component;
import java.awt.Graphics;
import java.awt.Rectangle;

import javax.swing.JComponent;

/**
 * Простейший пример реализации NodeRender.
 * 
 * @see NodeRender
 * 
 * @author dok
 * 
 */
public class CircleRender extends JComponent implements NodeRender {

    public Component getNodeRendererComponent(Object node, boolean pressed,
            boolean highlighted) {
        backgroundColor = (pressed) ? backgroun.darker() : backgroun;
        borderColor = (highlighted) ? border.brighter() : border;
        return this;
    }

    @Override
    public void paint(Graphics g) {
        Rectangle boundary = this.getBounds();
        g.setColor(borderColor);
        g.fillOval(0, 0, boundary.width, boundary.height);
        g.setColor(backgroundColor);
        g.fillOval(borderWidth, borderWidth, boundary.width - borderWidth * 2,
                boundary.height - borderWidth * 2);
    }

    private Color backgroundColor;

    private Color borderColor;

    private int borderWidth = 5;

    private Color backgroun = new Color(150, 200, 255);

    private Color border = new Color(70, 150, 255);

    private static final long serialVersionUID = -8249718835725058224L;
}
