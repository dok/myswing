package ru.dokwork.myswing;

import java.awt.BorderLayout;
import java.awt.Dimension;
import java.awt.GridLayout;
import java.awt.event.ActionEvent;

import javax.swing.AbstractAction;
import javax.swing.Action;
import javax.swing.BorderFactory;
import javax.swing.JButton;
import javax.swing.JFrame;
import javax.swing.JLabel;
import javax.swing.JPanel;
import javax.swing.JScrollPane;
import javax.swing.JSpinner;
import javax.swing.SpinnerNumberModel;
import javax.swing.SpringLayout;
import javax.swing.event.ChangeEvent;
import javax.swing.event.ChangeListener;
import javax.swing.JRadioButton;
import javax.swing.ButtonGroup;

/**
 * Окно демонстрационного примера использования компонента JGraphTree.
 * 
 * @author dok
 * 
 */
public class DemoWindow {

    /** Окно демонстрационного приложения */
    public JFrame frmJgraphtreeDemo;
    /** Компонент для представления дерева в виде графа */
    public final JGraphTree graphTree = new JGraphTree();
    /** Компонент для вывода текстовой информации */
    public JLabel labelComment;
    
    private JSpinner heightSpinner;
    private JSpinner widthSpinner;
    private final ButtonGroup buttonGroup = new ButtonGroup();
    private final Action actionExit = new ExitAction();
    private final Action actionSetButtonRender = new SetCircleRenderAction();
    private final Action actionSetCircleRender = new SetButtonRenderAction();
    private final CircleRender circleRender = new CircleRender();
    private final ButtonRender buttonRender = new ButtonRender();

    /**
     * Create the application.
     */
    public DemoWindow() {
        initialize();
    }

    /**
     * Initialize the contents of the frame.
     */
    @SuppressWarnings("serial")
    private void initialize() {
        frmJgraphtreeDemo = new JFrame();
        frmJgraphtreeDemo
                .setTitle("JGraphTree Demo http://www.dokwork.ru/2012/10/JGraphTree.html");
        frmJgraphtreeDemo.setBounds(100, 100, 550, 395);
        frmJgraphtreeDemo.setDefaultCloseOperation(JFrame.EXIT_ON_CLOSE);

        graphTree.setNodeDimention(new Dimension(100, 30));

        JPanel controlPanel = new JPanel();
        // controlPanel.setPreferredSize(new Dimension(170, 70));
        frmJgraphtreeDemo.getContentPane().add(controlPanel, BorderLayout.SOUTH);
        controlPanel.setLayout(new BorderLayout());

        final JScrollPane scrollPane = new JScrollPane();
        scrollPane.setViewportView(graphTree);
        frmJgraphtreeDemo.getContentPane().add(scrollPane, BorderLayout.CENTER);

        JPanel buttonPanel = new JPanel();
        buttonPanel.setPreferredSize(new Dimension(100, 40));
        controlPanel.add(buttonPanel, BorderLayout.EAST);
        SpringLayout sl_buttonPanel = new SpringLayout();
        buttonPanel.setLayout(sl_buttonPanel);

        JButton buttonExit = new JButton("Exit");
        sl_buttonPanel.putConstraint(SpringLayout.WEST, buttonExit, 5, SpringLayout.WEST, buttonPanel);
        sl_buttonPanel.putConstraint(SpringLayout.SOUTH, buttonExit, -5, SpringLayout.SOUTH, buttonPanel);
        sl_buttonPanel.putConstraint(SpringLayout.EAST, buttonExit, -5, SpringLayout.EAST, buttonPanel);
        buttonExit.setAction(actionExit);
        buttonPanel.add(buttonExit);

        JPanel panel = new JPanel();
        controlPanel.add(panel, BorderLayout.CENTER);
        panel.setLayout(new GridLayout(2, 0));

        heightSpinner = new JSpinner();
        heightSpinner.setModel(new GraphTreeSpinnerModel(
                graphTree.getNodeDimention().height, 0, 100, 1) {
            @Override
            public void stateChanged(ChangeEvent e) {
                Dimension nodeDimention = graphTree.getNodeDimention();
                nodeDimention.height = getValue();
                graphTree.setNodeDimention(nodeDimention);
            }
        });
        heightSpinner.setBorder(BorderFactory.createTitledBorder("Node height"));
        panel.add(heightSpinner);

        widthSpinner = new JSpinner();
        widthSpinner.setModel(new GraphTreeSpinnerModel(
                graphTree.getNodeDimention().width, 0, 200, 1) {
            @Override
            public void stateChanged(ChangeEvent e) {
                Dimension nodeDimention = graphTree.getNodeDimention();
                nodeDimention.width = getValue();
                graphTree.setNodeDimention(nodeDimention);
            }
        });
        widthSpinner.setBorder(BorderFactory.createTitledBorder("Node width"));
        panel.add(widthSpinner);
        
        JRadioButton rbButtonRender = new JRadioButton("Button render");
        rbButtonRender.setAction(actionSetCircleRender);
        buttonGroup.add(rbButtonRender);
        rbButtonRender.setSelected(true);
        panel.add(rbButtonRender);

        JSpinner vMarginSpinner = new JSpinner();
        vMarginSpinner.setModel(new GraphTreeSpinnerModel(graphTree.getVerticalMargin(),
                0, 100, 1) {
            @Override
            public void stateChanged(ChangeEvent e) {
                graphTree.setVerticalMargin(getValue());
            }
        });
        vMarginSpinner.setBorder(BorderFactory.createTitledBorder("Vert margin"));
        panel.add(vMarginSpinner);

        JSpinner hMarginSpinner = new JSpinner();
        hMarginSpinner.setModel(new GraphTreeSpinnerModel(
                graphTree.getHorizontalMargin(), 0, 100, 1) {
            @Override
            public void stateChanged(ChangeEvent e) {
                graphTree.setHorizontalMargin(getValue());
            }
        });
        hMarginSpinner.setBorder(BorderFactory
                .createTitledBorder("Hor margin"));
        panel.add(hMarginSpinner);
        
        JRadioButton rbCircleRender = new JRadioButton("Circle render");
        rbCircleRender.setAction(actionSetButtonRender);
        buttonGroup.add(rbCircleRender);
        panel.add(rbCircleRender);

        labelComment = new JLabel("");
        frmJgraphtreeDemo.getContentPane().add(labelComment, BorderLayout.NORTH);
    }

    @SuppressWarnings("serial")
    private class ExitAction extends AbstractAction {
        public ExitAction() {
            putValue(NAME, "Exit");
            putValue(SHORT_DESCRIPTION, "Close demo.");
        }

        public void actionPerformed(ActionEvent e) {
            System.exit(0);
        }
    }
    @SuppressWarnings("serial")
    private class SetCircleRenderAction extends AbstractAction {
        public SetCircleRenderAction() {
            putValue(NAME, "Circle render");
            putValue(SHORT_DESCRIPTION, "Change GraphTree.NodeRender to the CircleRender.");
        }
        public void actionPerformed(ActionEvent e) {
            graphTree.setNodeRender(circleRender);
            heightSpinner.setValue(30);
            widthSpinner.setValue(30);
        }
    }
    @SuppressWarnings("serial")
    private class SetButtonRenderAction extends AbstractAction {
        public SetButtonRenderAction() {
            putValue(NAME, "Button render");
            putValue(SHORT_DESCRIPTION, "Change GraphTree.NodeRender to the ButtonRender.");
        }
        public void actionPerformed(ActionEvent e) {
            graphTree.setNodeRender(buttonRender);
            heightSpinner.setValue(30);
            widthSpinner.setValue(100);
        }
    }

    private abstract class GraphTreeSpinnerModel extends SpinnerNumberModel implements
            ChangeListener {
        @Override
        public Integer getValue() {
            return (Integer) super.getValue();
        }

        public GraphTreeSpinnerModel(int val, int min, int max, int step) {
            super(val, min, max, step);
            addChangeListener(this);
        }

        public abstract void stateChanged(ChangeEvent e);

        private static final long serialVersionUID = 219212509487559532L;
    }
}
