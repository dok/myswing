package ru.dokwork.myswing;

import java.awt.event.MouseEvent;

/**
 * Описывает событие взаимодействия мыши с узлом дерева.
 * 
 * @author dok
 * 
 */
public class MouseNodeEvent {    

    public Object getNode() {
        return node;
    }

    public MouseEvent getMouseEvent() {
        return mevent;
    }

    public MouseNodeEvent(Object node, MouseEvent event) {
        this.node = node;
        this.mevent = event;
    }
    
    private Object node;
    
    private MouseEvent mevent;
}
