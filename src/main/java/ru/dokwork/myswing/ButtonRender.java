package ru.dokwork.myswing;

import java.awt.Component;

import javax.swing.JButton;

/**
 * Отрисовывает узел дерева в виде стандартной кнопки.
 * 
 * @author dok
 * 
 */
public class ButtonRender implements NodeRender {

    /**
     * Возвращает кнопку, по шаблону которой будут отображаться узлы дерева.
     */
    public JButton getButton() {
        return button;
    }

    /**
     * Возвращает экземпляр JButton, рисующий узел дерева.
     * 
     * @param node
     *            отображаемый узел дерева.
     * @param pressed
     *            true, если узел находится в нажатом состоянии.
     * @param highlighted
     *            true, если узел подсвечен.
     * @return JButton, рисующий узел дерева.
     */
    @Override
    public Component getNodeRendererComponent(Object node, boolean selected,
            boolean highlighted) {
        button.setText(node.toString());
        button.getModel().setRollover(highlighted);
        button.getModel().setPressed(selected);
        button.getModel().setArmed(selected);
        return button;
    }

    /**
     * Создает компонент для отрисовки узла дерева в виде кнопки.
     */
    public ButtonRender() {
        button = new JButton();
    }

    private JButton button;
}
