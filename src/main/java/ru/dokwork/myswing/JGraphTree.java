package ru.dokwork.myswing;

import java.awt.Dimension;
import java.awt.Graphics;
import java.awt.Point;
import java.awt.Rectangle;
import java.beans.PropertyChangeEvent;
import java.beans.PropertyChangeListener;

import javax.swing.JComponent;
import javax.swing.tree.DefaultMutableTreeNode;
import javax.swing.tree.DefaultTreeModel;
import javax.swing.tree.TreeModel;

/**
 * Компонент представляет дерево в виде графа.
 * 
 * @author dok
 * @see http://www.dokwork.ru/2012/10/JGraphTree.html
 * 
 */
public class JGraphTree extends JComponent {

    /**
     * Возвращает размер узла дерева.
     * 
     * @return размер узла дерева.
     */
    public Dimension getNodeDimention() {
        return treeMap.getNodeSize();
    }

    /**
     * Устанавливает размер узла дерева.
     * 
     * @param nodeDimention
     *            размер узла дерева.
     */
    public void setNodeDimention(Dimension nodeDimention) {
        treeMap.setNodeSize(nodeDimention);
    }

    /**
     * Возвращает вертикальный отступ между узлами.
     * 
     * @return вертикальный отступ между узлами.
     */
    public int getVerticalMargin() {
        return treeMap.getVerticalMargin();
    }

    /**
     * Устанавливает вертикальный отступ между узлами.
     * 
     * @param val
     *            вертикальный отступ между узлами.
     */
    public void setVerticalMargin(int val) {
        treeMap.setVerticalMargin(val);
    }

    /**
     * Возвращает горизонтальный отступ между узлами
     * 
     * @return горизонтальный отступ между узлами
     */
    public int getHorizontalMargin() {
        return treeMap.getHorizontalMargin();
    }

    /**
     * Устанавливает горизонтальный отступ между узлами
     * 
     * @param val
     *            горизонтальный отступ между узлами
     */
    public void setHorizontalMargin(int val) {
        treeMap.setHorizontalMargin(val);
    }

    public TreeModel getModel() {
        return treeMap.getTreeModel();
    }

    /**
     * Устанавливает модель дерева.
     * 
     * @param model
     *            модель дерева.
     */
    public void setModel(TreeModel model) {
        treeMap.setTreeModel(model);
    }

    /**
     * Возвращает узел дерева, который содержит указанную точку, или null, если точка не
     * попадает ни в один узел.
     * 
     * @param p
     *            точка на компоненте.
     * @return узел дерева, который содержит указанную точку, или null, если точка не
     *         попадает ни в один узел.
     */
    public Object getNode(Point p) {
        return treeMap.getNode(p);
    }

    public NodeRender getNodeRender() {
        if (ui instanceof GraphTreeRender) {
            GraphTreeRender treeRender = (GraphTreeRender) ui;
            return treeRender.nodeRender;
        }
        return null;
    }

    public void setNodeRender(NodeRender render) {
        if (render == null) {
            throw new IllegalArgumentException("Render can't be null.");
        }
        if (ui instanceof GraphTreeRender) {
            GraphTreeRender treeRender = (GraphTreeRender) ui;
            treeRender.nodeRender = render;
            repaint();
        }
    }

    /**
     * Добавляет слушателя событий, связанных с взаимодействием мыши с узлами дерева.
     * 
     * @param l
     *            слушатель событий, связанных с взаимодействием мыши с узлами дерева.
     */
    public void addMouseNodeEventListener(MouseNodeEventListener l) {
        mouseAgent.addListener(l);
    }

    /**
     * Удаляет слушателя событий, связанных с взаимодействием мыши с узлами дерева.
     * 
     * @param l
     *            слушатель событий, связанных с взаимодействием мыши с узлами дерева.
     */
    public void removeMouseNodeEventListener(MouseNodeEventListener l) {
        mouseAgent.removeListener(l);
    }

    public JGraphTree() {
        this(new ButtonRender(), new Dimension(50, 50));
    }

    public JGraphTree(NodeRender render, Dimension nodeDimention) {
        super();
        this.setUI(new GraphTreeRender(render));

        this.treeMap = new TreeMap();
        this.treeMap.addPropertyChangeListener(new PropertyChangeListener() {
            public void propertyChange(PropertyChangeEvent evt) {
                if (evt.getPropertyName().equals(TreeMap.TREE_CHANGED_EVENT)) {
                    setPreferredSize(treeMap.getSize());
                    revalidate();
                    repaint();
                }
            }
        });
        this.treeMap.setNodeSize(nodeDimention);
        this.treeMap.setTreeModel(createDefaultTreeModel());

        this.mouseAgent = new MouseAgent(this);
        this.addMouseListener(mouseAgent);
        this.addMouseMotionListener(mouseAgent);
    }

    protected void repaint(Object node, Graphics g) {
        if (ui instanceof GraphTreeRender) {
            GraphTreeRender render = (GraphTreeRender) ui;
            Rectangle boundary = treeMap.getNodeBoundary(node);
            render.paintNode(node, g, boundary, node == pressedNode,
                    node == highlightedNode);
        }
    }

    /**
     * Создает демонстрационное дерево по умолчанию.
     */
    private static TreeModel createDefaultTreeModel() {
        DefaultMutableTreeNode[] node = new DefaultMutableTreeNode[11];
        for (int i = 0; i < node.length; i++) {
            node[i] = new DefaultMutableTreeNode("" + i);
        }
        node[4].add(node[8]);
        node[4].add(node[9]);
        node[4].add(node[10]);
        node[4].add(node[10]);
        node[3].add(node[6]);
        node[3].add(node[7]);
        node[1].add(node[4]);
        node[1].add(node[5]);
        node[0].add(node[1]);
        node[0].add(node[2]);
        node[0].add(node[3]);
        return new DefaultTreeModel(node[0]);
    }

    protected Object highlightedNode = null;

    protected Object pressedNode = null;

    protected TreeMap treeMap;

    private MouseAgent mouseAgent;

    private static final long serialVersionUID = -2665909215980982613L;
}
