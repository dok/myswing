package ru.dokwork.myswing;

import java.awt.Point;
import java.awt.event.MouseAdapter;
import java.awt.event.MouseEvent;
import java.util.ArrayList;
import java.util.List;

/**
 * Отслеживает события мыши и генерирует события взаимодействия мыши с узлами.
 * 
 * @author dok
 * 
 */
class MouseAgent extends MouseAdapter implements MouseNodeEventListener {

    @Override
    public void mouseMoved(MouseEvent e) {
        Object node = tree.getNode(e.getPoint());
        /* Мышью двинули по подсвеченному узлу */
        if (node == tree.highlightedNode) {
            return;
        }
        if (tree.highlightedNode != null) {
            MouseNodeEvent event = new MouseNodeEvent(tree.highlightedNode, e);
            tree.highlightedNode = null;
            fireMouseExitedEvent(event);
        }
        /* Мышь за пределами узла, нажатие прекращается */
        if (node == null) {
            tree.pressedNode = null;
        }
        /* Мышь над узлом, отличным от выделенного - выделен новый узел */
        if (node != null) {
            tree.highlightedNode = node;
            MouseNodeEvent event = new MouseNodeEvent(node, e);
            fireMouseEnteredEvent(event);
        }
    }

    @Override
    public void mousePressed(MouseEvent e) {
        if (e.getButton() != MouseEvent.BUTTON1) {
            return;
        }
        Point p = e.getPoint();
        Object node = tree.getNode(p);
        tree.pressedNode = node;
        if (node == null) {
            return;
        }
        MouseNodeEvent event = new MouseNodeEvent(node, e);
        fireMousePressedEvent(event);
    }

    @Override
    public void mouseReleased(MouseEvent e) {
        if (e.getButton() != MouseEvent.BUTTON1) {
            return;
        }
        Point p = e.getPoint();
        Object node = tree.getNode(p);
        tree.pressedNode = null;
        if (node == null) {
            return;
        }
        MouseNodeEvent event = new MouseNodeEvent(node, e);
        fireMouseReleasedEvent(event);
    }

    @Override
    public void mouseClicked(MouseEvent e) {
        if (e.getButton() != MouseEvent.BUTTON1) {
            return;
        }
        Point p = e.getPoint();
        Object node = tree.getNode(p);
        if (node == null) {
            return;
        }
        MouseNodeEvent event = new MouseNodeEvent(node, e);
        fireMouseClickedEvent(event);
    }

    public void addListener(MouseNodeEventListener l) {
        listeners.add(l);
    }

    public void removeListener(MouseNodeEventListener l) {
        listeners.remove(l);
    }

    /* При любом взаимодействии с узлом, происходит его перерисовка */
    public void mousePressed(MouseNodeEvent event) {
        tree.repaint(event.getNode(), tree.getGraphics());
    }

    public void mouseReleased(MouseNodeEvent event) {
        tree.repaint(event.getNode(), tree.getGraphics());
    }

    public void mouseEntered(MouseNodeEvent event) {
        tree.repaint(event.getNode(), tree.getGraphics());
    }

    public void mouseExited(MouseNodeEvent event) {
        tree.repaint(event.getNode(), tree.getGraphics());
    }
    
    public void mouseClicked(MouseNodeEvent event) {
    }

    MouseAgent(JGraphTree tree) {
        this.tree = tree;
        listeners = new ArrayList<MouseNodeEventListener>();
        addListener(this);
    }

    private void fireMouseEnteredEvent(MouseNodeEvent event) {
        for (MouseNodeEventListener l : listeners) {
            l.mouseEntered(event);
        }
    }

    private void fireMouseExitedEvent(MouseNodeEvent event) {
        for (MouseNodeEventListener l : listeners) {
            l.mouseExited(event);
        }
    }

    private void fireMousePressedEvent(MouseNodeEvent event) {
        for (MouseNodeEventListener l : listeners) {
            l.mousePressed(event);
        }
    }

    private void fireMouseReleasedEvent(MouseNodeEvent event) {
        for (MouseNodeEventListener l : listeners) {
            l.mouseReleased(event);
        }
    }
    
    private void fireMouseClickedEvent(MouseNodeEvent event) {
        for (MouseNodeEventListener l : listeners) {
            l.mouseClicked(event);
        }
    }

    private JGraphTree tree;

    private List<MouseNodeEventListener> listeners;
}
