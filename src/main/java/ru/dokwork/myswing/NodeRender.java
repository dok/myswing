package ru.dokwork.myswing;

import java.awt.Component;

/**
 * Интерфейс отрисовки узла дерева.
 * 
 * @author dok
 * 
 */
public interface NodeRender {

    /**
     * Возвращает компонент, рисующий узел дерева.
     * 
     * @param node
     *            отображаемый узел дерева.
     * @param pressed
     *            true, если узел находится в нажатом состоянии.
     * @param highlighted
     *            true, если узел подсвечен.
     * @return компонент, рисующий узел дерева.
     */
    Component getNodeRendererComponent(Object node, boolean pressed, boolean highlighted);
}
