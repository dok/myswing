package ru.dokwork.myswing;

import static org.junit.Assert.assertEquals;
import static org.junit.Assert.fail;

import javax.swing.tree.DefaultMutableTreeNode;
import javax.swing.tree.DefaultTreeModel;
import javax.swing.tree.TreeModel;

import org.junit.Before;
import org.junit.Test;

public class JMyTreeTests {

    @Before
    public void setUp() throws Exception {
        // ______0
        // _____/|\
        // ____1_2_3
        // ___/_\___|\
        // __4___5__6_7
        // _/_|_\
        // 8__9_10
        DefaultMutableTreeNode[] node = new DefaultMutableTreeNode[11];
        for (int i = 0; i < node.length; i++) {
            node[i] = new DefaultMutableTreeNode("" + i);
        }
        node[4].add(node[8]);
        node[4].add(node[9]);
        node[4].add(node[10]);
        node[3].add(node[6]);
        node[3].add(node[7]);
        node[1].add(node[4]);
        node[1].add(node[5]);
        node[0].add(node[1]);
        node[0].add(node[2]);
        node[0].add(node[3]);
        model = new DefaultTreeModel(node[0]);
    }

    @Test
    public void testCalculateSize() {
        JGraphTree tree = new JGraphTree();
        tree.setModel(model);
        int height = 4 * tree.getNodeDimention().height + 3 * tree.getVerticalMargin();
        int width = 7 * tree.getNodeDimention().width + 6 * tree.getHorizontalMargin();
        assertEquals("Не совпала высота компонента.", tree.getPreferredSize().height,
                height);
        assertEquals("Не совпала ширина компонента.", tree.getPreferredSize().width,
                width);
    }

    @Test
    public void simpleTestCalculateSize() {
        DefaultMutableTreeNode node = new DefaultMutableTreeNode();
        DefaultMutableTreeNode node2 = new DefaultMutableTreeNode();
        node2.add(new DefaultMutableTreeNode());
        node2.add(new DefaultMutableTreeNode());
        node.add(node2);
        model = new DefaultTreeModel(node);
        JGraphTree tree = new JGraphTree();
        tree.setModel(model);
        int height = 3 * tree.getNodeDimention().height + 2 * tree.getVerticalMargin();
        int width = 2 * tree.getNodeDimention().width + tree.getHorizontalMargin();
        assertEquals("Не совпала высота компонента.", tree.getPreferredSize().height,
                height);
        assertEquals("Не совпала ширина компонента.", tree.getPreferredSize().width,
                width);
    }

    @Test
    public void testGetTreeNode() {
        fail("Not yet implemented");
    }

    private TreeModel model;
}
